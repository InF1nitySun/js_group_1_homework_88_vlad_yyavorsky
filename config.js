module.exports = {
  db : {
    url : 'mongodb://localhost:27017',
    name : 'forum'
  },
  token: {
    lifetime: 70000,
    secret: 'secret'
  }
};