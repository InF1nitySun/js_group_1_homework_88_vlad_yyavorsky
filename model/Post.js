const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User'
    },
    title: {
        type: String, required: true
    },
    description: {
        type: String
    },
    image: {
        type: String
    },
    status: {
        type: String, required: true
    }
});


mongoose.model('Post', PostSchema);

module.exports = mongoose.model('Post');